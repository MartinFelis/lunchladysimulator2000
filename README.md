Copyright (c) 2013 Martin Felis <martin@fysx.org>

Entry for the 28th Ludum Dare Game Jam.

Cookie Time
===========

This game is about cookies, righteousness, and well deserved punishment.
Every day at lunch time the school kids get a cookie, following the golden
rule: you only get one. However those pesky sneaky evil kids (as we all
know them) however will try to get more than one. This outrageous evilness
has to stop, and there is only one person with the power:

    You, the lunch lady!

Thanks for reading! Here, have a cookie ... erm mockup!

Credits
=======

* punch.wav - by Ekokubza123, published under CC-0 at http://freesound.org/people/Ekokubza123/sounds/104183/
* whips.mp3 - by animationIsaac, published under CC-0 at http://www.freesound.org/people/animationIsaac/sounds/149897/
* whipping.wav - by SoundsExciting, published under CC-0 at http://www.freesound.org/people/SoundsExciting/sounds/204358/
* crunching.flac - junkfood2121, published under CC-0 at http://www.freesound.org/people/junkfood2121/sounds/202145/
* cafeteria.mp3 - Cyberkineticfilms, published under CC-0 at http://www.freesound.org/people/Cyberkineticfilms/sounds/128291/

About
=====

This game is made using the fabulous Löve2d game engine
([http://love2d.org]([http://love2d.org])). Also I used lots of helper
utilities made by ([http://vrld.org]([http://vrld.org)).

Have fun!

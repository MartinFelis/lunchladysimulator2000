local students = { }

local function _NOP_() end

-- Usage:
-- class{function(self)
--     STUFF()
--     Entities.add(self)
-- end}
local function add(entity)
	students[entity] = assert(entity, "No entity to add")
end

local function remove(entity, ...)
	assert(entity, "No entity to remove")
	students[entity] = nil
	(entity.finalize or _NOP_)(entity, ...)
end

local function clear(...)
	for e in pairs(students) do
		remove(e, ...)
	end
end

local function size()
	local n = 0
	for e in pairs(students) do
		n = n + 1
	end

	return n
end

-- Usage:
-- Entities.update(dt)
-- Entities.draw()
-- ...
return setmetatable({
	register = add,
	add      = add,
	remove   = remove,
	clear    = clear,
	items    = students,
	size    = size,
}, {__index = function(_, func)
	return function(...)
		for e in pairs(students) do
			(e[func] or _NOP_)(e,...)
		end
	end
end})

local cookies = { }

local function _NOP_() end

-- Usage:
-- class{function(self)
--     STUFF()
--     Entities.add(self)
-- end}
local function add(entity)
	cookies[entity] = assert(entity, "No entity to add")
end

local function remove(entity, ...)
	assert(entity, "No entity to remove")
	cookies[entity] = nil
	(entity.finalize or _NOP_)(entity, ...)
end

local function clear(...)
	for e in pairs(cookies) do
		remove(e, ...)
	end
end

local function size()
	local n = 0
	for e in pairs(cookies) do
		n = n + 1
	end

	return n
end

-- Usage:
-- Entities.update(dt)
-- Entities.draw()
-- ...
return setmetatable({
	register = add,
	add      = add,
	remove   = remove,
	clear    = clear,
	items    = cookies,
	size    = size,
}, {__index = function(_, func)
	return function(...)
		for e in pairs(cookies) do
			(e[func] or _NOP_)(e,...)
		end
	end
end})

local st = GS.new()

local shape_1 = {
	-100, -50,
	-80, 50,
	60, 100, 
	100, -50,
	15, -110
}

function st:updateAndDrawDebugVars()
	love.graphics.setFont(debug_font)
	love.graphics.setColor (255, 255, 255, 255)

	Gui.group.push{grow = "down", pos = {20, 20}}
	for k,var in pairs (debug_variables) do
		if tonumber(var) ~= nil then
			Gui.Label{text = ("%s: %.2f"):format(k, var)}
		elseif #var == 2 and tonumber(var[1]) ~= nil and tonumber(var[2]) ~= nil then
			Gui.Label{text = ("%s: %.2f, %.2f"):format(k, var[1], var[2])}
		else
			Gui.Label{text = tostring(k) .. ": " .. tostring (var)}
		end
	end
	Gui.group.pop{}
end


function st:init()
end

function st:enter()
	self.interrupt_time = 0
	self.pos = vector(0,0)
	self.camVelocity = vector(0,0)

	self.cam = Camera()
	self.cam:lookAt(self.pos:unpack())
	local mousepos = vector(self.cam:mousepos())
end

function st:leave()
end

local function draw_shape_rot(angle, scale)
	love.graphics.push()
	love.graphics.scale (scale)
	love.graphics.rotate (math.sin(angle))

	love.graphics.setColor(255,255,255)
	love.graphics.polygon ('fill', shape_1)
	love.graphics.setColor(60, 60, 60)
	love.graphics.setLineWidth (4)
	love.graphics.polygon ('line', shape_1)
	love.graphics.pop()
end

function st:update(dt)
	self.interrupt_time = self.interrupt_time + dt
	local mousepos = vector(self.cam:mousepos())

	self.camVelocity = mousepos - self.pos

--	self.pos = self.pos - self.camVelocity * dt
	debug_variables["pos"] = self.pos
	debug_variables["mouse_pos"] = mousepos

	self.cam:lookAt(self.pos:unpack())
end

function st:draw()
	self.cam:attach()

	love.graphics.setColor(0,0,0,180)
	love.graphics.rectangle('fill', 0,0,SCREEN_WIDTH,SCREEN_HEIGHT)

	love.graphics.setColor(255, 0, 0, 255)
	love.graphics.circle ('fill', self.pos.x, self.pos.y, 10)

	love.graphics.push()
	love.graphics.scale(1.8)

	draw_shape_rot(self.interrupt_time * 0.2 + 0.3, 1.3 + 0.1 * math.sin(self.interrupt_time * 1.6 + 0.3))
	draw_shape_rot(self.interrupt_time * 0.1 + 0.9, 1.3 + 0.1 * math.sin(self.interrupt_time * 1.6 + 0.1))
	draw_shape_rot(self.interrupt_time * 0.3 + 0.7, 1.3 + 0.1 * math.sin(self.interrupt_time * 1.6 + 0.6))

	love.graphics.push()
	love.graphics.scale (0.8 + 0.05 * math.sin(self.interrupt_time * 1.6 + 0.6))

	love.graphics.setFont(message_font)

	love.graphics.printf("ABOUT", -100, -80, 200, 'center')
	love.graphics.printf("This is a LD48 entry made by", -200, -50, 400, 'center')
	love.graphics.printf("Martin Felis (@fysxdotorg)", -200, -30, 400, 'center')
	love.graphics.printf("Using Löve2D and lots of", -200, 0, 400, 'center')
	love.graphics.printf("helper libs created by VRLD", -200, 20, 400, 'center')
	love.graphics.printf("and the Regen font by", -200, 40, 400, 'center')
	love.graphics.printf("Pixel Sagas", -200, 60, 400, 'center')
	love.graphics.pop()

	love.graphics.pop()

	self.cam:detach()

	Gui.group.push{grow = "right", pos = {SCREEN_WIDTH * 0.5 - 50,SCREEN_HEIGHT - 54}}
	if Gui.Button{text = "Menu"} then
		GS.switch(State.menu)
	end
	Gui.group.pop()

	Gui.core.draw()
end

function st:keyreleased(key)
	if key == 'escape' then
		love.event.push("quit")
	end
end

function st:keypressed(key, code)
  Gui.keyboard.pressed(key, code)
end

return st

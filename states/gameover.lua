local st = GS.new()

function st:enter()
	local window_size = vector(love.window.getDimensions())

	self.student = Entity.student(vector(window_size.x * 0.8, window_size.y * 0.6))

	self.cookie_pos = vector( 
		window_size.x * 0.65, 
		window_size.y - window_size.y * 0.2
	)

	self.slap = {
		start_time = 0
	}
end

function st:draw()
	local window_size = vector(love.window.getDimensions())

	local background_draw_scale = vector(
	window_size.x / Image.background:getWidth(),
	window_size.y / Image.background:getHeight()
	)

	love.graphics.setColor(255, 255, 255)

	love.graphics.draw(Image.background, 0, 0, 0, background_draw_scale.x, background_draw_scale.y)

	self.student:draw()

	-- counter
	love.graphics.setColor(255, 255, 255)
	local table_draw_scale = vector(
	window_size.x / Image.table:getWidth(),
	window_size.y * 0.4 / Image.table:getHeight()
	)

	love.graphics.draw(Image.table, 0, window_size.y * 0.6, 0, table_draw_scale.x, table_draw_scale.y)

	local cookie_draw_scale = vector(
	80 / Image.cookie:getWidth(),
	80 / Image.cookie:getHeight()
	)

	love.graphics.setColor (255, 255, 255)
	love.graphics.draw(Image.cookie, self.cookie_pos.x, self.cookie_pos.y, 0, cookie_draw_scale.x, cookie_draw_scale.y)

	love.graphics.setColor (0, 0, 0)
	love.graphics.setFont (Font.Hachicro[40])
	love.graphics.printf("- GAME OVER -", 50, window_size.y * 0.1, 2000, "left")

	love.graphics.setFont (instruction_font)
	love.graphics.printf("Score: " .. tostring(State.game.score), 50, window_size.y * 0.2, 2000, "left")

	local reason = ""
	if State.game.hungry_students >= 5 then
		reason = "Too many whiny students complained that they did not get a cookie. How could this happen?!?"
	end

	if State.game.evil_students_fed >= 5 then
		reason = "'You only get one!' How hard is that?!? Seems like you gave a lot of students more than one. This is unacceptable!"
	end

	if State.game.innocents_slapped >= 5 then
		reason = "I do agree that students are annoying, however slapping so many publicly is NOT acceptable!"
	end
		
	love.graphics.setFont (instruction_font)
		
	love.graphics.printf(reason, 50, window_size.y * 0.3, 650, "left")

	love.graphics.setColor (0, 0, 0)
	love.graphics.setFont(instruction_font)
	local instructions = "Give cookie to student\n    - OR -\nSlap student"
	love.graphics.printf(instructions, 50, window_size.y * 0.7, 800, "left")

	self.student:drawArmsAndHead()

	Timer.update(0)
end

function st:resize(x,y)
	st:enter()
end

function st:update(dt)
	if self.cookie_in_hand then
		local mouse = vector (love.mouse.getPosition())
		self.cookie_pos = mouse + vector(-40, -40)
		self.student:setCookiePosition(mouse.x, mouse.y)
	end

	if (self.cookie_pos - self.student:getHandPosition()):len() < 100 then
		self.cookie_in_hand = false

		local crunching_sounds = {
			Sound.static.crunching1,
			Sound.static.crunching2,
			Sound.static.crunching3,
		}

		love.audio.play(select_sound_source(crunching_sounds))
		GS.switch(State.game)
	end
end

function st:keyreleased(key)
	if key == 'escape' then
		GS.switch(State.menu)
	elseif key == 'return' then
		love.audio.play(Sound.static.whip2)
		GS.switch(State.game)
	end
end

function st:mousepressed (x, y, button)
	if (vector(x,y) - self.cookie_pos):len() < 100 then
		self.cookie_in_hand = true
	else 
		self.cookie_in_hand = false
	end

	if not self.cookie_in_hand then
		self.slap.start_time = love.timer.getTime()
		self.slap.start_pos = vector(x,y)
	end
end

function st:mousereleased (x, y, button)
	if not self.cookie_in_hand then
		local slap = self.slap

		local slap_duration = love.timer.getTime() - slap.start_time
		slap.end_pos = vector(x,y)

		if slap_duration < 0.3 and (slap.end_pos - slap.start_pos):len() > 100 then
			local was_slapped = false
			if self.student:isSlapped(slap.start_pos, slap.end_pos) then
				was_slapped = true
				love.audio.play(Sound.static.punch3)
				GS.switch(State.game)
			end

			if not was_slapped then
				local alpha = 255;
				Timer.do_for (0.3, function(dt)
					alpha = alpha - dt * 255 / 0.3
					love.graphics.setColor (255., 255., 255., alpha)
					love.graphics.line (slap.start_pos.x, slap.start_pos.y, slap.end_pos.x, slap.end_pos.y)
				end)

				local whipping_sounds = {
					Sound.static.whip1,
					Sound.static.whip2,
					Sound.static.whip3,
				}

				love.audio.play(select_sound_source(whipping_sounds))
			end
		end

		slap.start_time = 0
		return
	end

	self.cookie_in_hand = false
--	self:enter()
end



return st

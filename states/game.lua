local st = GS.new()

local mouse = vector(0,0)
local window_size = vector(love.window.getDimensions())
local cookie_in_hand = false
local world = {}
local counter_body = {}
local slap = {
	start_time = 0,
	start_pos = {}
}

local camera = Camera()

function st:event_award_points(points, pos)
	self.score = self.score + points

	Timer.do_for (0.5, function (dt)
		if points < 0 then
			love.graphics.setColor(255, 0, 0)
		else
			love.graphics.setColor(255, 255, 255)
		end

		love.graphics.setFont(points_font)
		--			point_position.x = point_position.x + dt * student.vel.x
		pos.y = pos.y - dt * 200 
		love.graphics.printf(tostring(points), pos.x, pos.y, 500, "left")
	end)
end

function st:event_got_cookie(student)
	local crunching_sounds = {
		Sound.static.crunching1,
		Sound.static.crunching2,
		Sound.static.crunching3,
	}

	love.audio.play(select_sound_source(crunching_sounds))

	if student.is_evil and student.got_cookie then
		self.signals:emit('award_points', -50, student:getHandPosition())
		self.evil_students_fed = self.evil_students_fed + 1
	else
		self.signals:emit('award_points', 10, student:getHandPosition())
		student.is_happy = true
	end

	student.got_cookie = true
	cookie_in_hand = false
end

function st:event_slap(student, slap_start, slap_end)
	local punch_sounds = {
		Sound.static.punch1,
		Sound.static.punch2,
		Sound.static.punch3,
	}

	love.audio.play(select_sound_source(punch_sounds))

	student.is_slapped = true

	local alpha = 255
	Timer.do_for (0.15, function(dt)
		alpha = alpha - dt * 255 / 0.15
		love.graphics.setColor (255., 255., 255., alpha)
		love.graphics.line (slap_start.x, slap_start.y, slap_end.x, slap_end.y)
	end)
	if student.is_evil then
		self.signals:emit('award_points', 50, student:getHeadPosition())
	else
		self.innocents_slapped = self.innocents_slapped + 1
		self.signals:emit('award_points', -100, student:getHeadPosition())
	end

	student.body:setActive (false)

	local whip_dir = 1.
	if slap_end.x - slap_start.x < 0 then
		whip_dir = -1
	end

	Timer.do_for (1., function(dt)
		student.angle = student.angle + whip_dir * dt * 2.
		student.pos.x = student.pos.x + whip_dir * dt * 200
		student.pos.y = student.pos.y + dt * 200
	end)

	Timer.add (1.1, function(dt)
		students.remove(student)
	end)
end

function st:event_whip(slap_start, slap_end)
	local alpha = 255;
	Timer.do_for (0.3, function(dt)
		alpha = alpha - dt * 255 / 0.3
		love.graphics.setColor (255., 255., 255., alpha)
		love.graphics.line (slap_start.x, slap_start.y, slap_end.x, slap_end.y)
	end)

	local whipping_sounds = {
		Sound.static.whip1,
		Sound.static.whip2,
		Sound.static.whip3,
	}

	love.audio.play(select_sound_source(whipping_sounds))
end

function st:spawnStudent()
	local student = Entity.student(vector(-200, 400))
	student:initPhysics(world)

	student.greed_factor = 1. + 0.5 * (love.math.random() * 2 - 1.)
	student.default_velocity = 250 * self.difficulty
	if love.math.random() > 0.7 then
		student.is_evil = true
--		student.skin_color = {255, 0, 0}
	end

	students.add(student)
	self.last_student = love.timer.getTime()
	return student
end

function st:enter()
	window_size = vector(love.window.getDimensions())

	self.signals = Signal.new()

	self.signals:register('got_cookie', function (student) self:event_got_cookie(student) end)
	self.signals:register('slap', function (student, start_pos, end_pos) self:event_slap(student, start_pos, end_pos) end)
	self.signals:register('whip', function(start_pos, end_pos) self:event_whip(start_pos, end_pos) end)
	self.signals:register('award_points', function(points, pos) self:event_award_points(points, pos) end)

	camera:lookAt(window_size.x * 0.5, window_size.y * 0.5)

	students.clear()

	love.physics.setMeter (300)
	world = love.physics.newWorld(0, 1000., true)
	counter_body = love.physics.newBody(world, 0, 0, "static")

	local counter_shape = love.physics.newRectangleShape(window_size.x * 4., window_size.y * 0.25)
	local counter_fixture = love.physics.newFixture (counter_body, counter_shape)

	local counter_position = vector (window_size.x * 0.5, window_size.y * 0.83)

	counter_body:setPosition(counter_position.x, counter_position.y)

	Sound.stream.ambient:play()
	Sound.stream.ambient:setLooping(true)
	Sound.stream.ambient:setVolume(0.2)

	self.cookie_box = Entity.cookiebox({100, 100}, 0.2)
	self.score = 0
	self.last_student = 0
	self.student_rate = 1.
	self.difficulty = 1.
	self.evil_students_fed = 0
	self.hungry_students = 0
	self.innocents_slapped = 0
end

function st:leave()
	Sound.stream.ambient:stop()
end

function st:drawHUD()
	love.graphics.setColor(80, 80, 80)
	love.graphics.rectangle ('fill', 0, 0, window_size.x, 32)

	love.graphics.setFont(score_font)
	if self.score < 0 then
		love.graphics.setColor (255, 20, 20)
	else
		love.graphics.setColor (255, 255, 255)
	end

	love.graphics.printf("Score: " .. tostring (self.score), window_size.x - score_font:getWidth ("Score: " .. tostring(self.score)) - 10, 8, 1000, "left")

	if self.innocents_slapped > 3 then
		love.graphics.setColor (255, 0, 0)
	elseif self.innocents_slapped > 2 then
		love.graphics.setColor (255, 128, 0)
	else
		love.graphics.setColor (0, 0, 0)
	end

	love.graphics.print("Innocents slapped: " .. tostring (self.innocents_slapped), 20, window_size.y - 120)

	if self.hungry_students > 3 then
		love.graphics.setColor (255, 0, 0)
	elseif self.hungry_students > 2 then
		love.graphics.setColor (255, 128, 0)
	else
		love.graphics.setColor (0, 0, 0)
	end

	love.graphics.print("Hungry Students:   " .. tostring (self.hungry_students), 20, window_size.y - 80)

	if self.evil_students_fed > 3 then
		love.graphics.setColor (255, 0, 0)
	elseif self.evil_students_fed > 2 then
		love.graphics.setColor (255, 128, 0)
	else
		love.graphics.setColor (0, 0, 0)
	end

	love.graphics.print("Sneaky Students:   " .. tostring (self.evil_students_fed), 20, window_size.y - 40)
end

function st:draw()
	window_size = vector(love.window.getDimensions())
	camera:lookAt(window_size.x * 0.5, window_size.y * 0.5)
	camera:zoomTo(1.0)
	camera:attach()

	local background_draw_scale = vector(
	window_size.x / Image.background:getWidth(),
	window_size.y / Image.background:getHeight()
	)

	love.graphics.setColor (255, 255, 255)
	love.graphics.draw(Image.background, 0, 0, 0, background_draw_scale.x, background_draw_scale.y)

	--love.graphics.setBackgroundColor(90, 90, 90)
	
	self.cookie_box.pos = vector (window_size.x - 200 - self.cookie_box.size.x * 0.,window_size.y - self.cookie_box.size.y * 0.5 - 50)

	students.draw()

	love.graphics.setColor (255, 255, 255)

	-- counter
	local table_draw_scale = vector(
	window_size.x / Image.table:getWidth(),
	window_size.y * 0.4 / Image.table:getHeight()
	)

	love.graphics.draw(Image.table, 0, window_size.y * 0.6, 0, table_draw_scale.x, table_draw_scale.y)
	self.cookie_box:draw()

	if cookie_in_hand then
		local cookie_draw_scale = vector(
		80 / Image.cookie:getWidth(),
		80 / Image.cookie:getHeight()
		)

		love.graphics.setColor (255, 255, 255)
		love.graphics.draw(Image.cookie, mouse.x - 40, mouse.y -40, 0, cookie_draw_scale.x, cookie_draw_scale.y)
	end

	for k,s in pairs(students.items) do
		if not s.is_slapped then
			s:drawArmsAndHead()
		end
	end

	Timer.update(0)
	camera:detach()
	self:drawHUD()
end

function st:update(dt)
	if self.evil_students_fed >= 5 
		or self.hungry_students >= 5
		or self.innocents_slapped >= 5
		then
		GS.switch(State.gameover)
	end

--	Timer.update(dt)
	mouse = vector (camera:worldCoords(love.mouse.getPosition()))

	if cookie_in_hand then
		students.setCookiePosition(mouse.x, mouse.y)
	else
	end
	students.setDefaultVelocity ()

	if (love.timer.getTime() - self.last_student) > self.student_rate / self.difficulty then
		self:spawnStudent()
	end

	self.cookie_box:update(dt)
	world:update(dt)
	students.update(dt)

	for k,s in pairs(students.items) do
		if cookie_in_hand and not s.is_slapped and not s.is_happy and (mouse - s:getHandPosition()):len() < s.head_radius * 0.5 then
			self.signals:emit('got_cookie', s)
		end

		if s.pos.x > window_size.x + s.size.x then
			if not s.is_happy then
				self.hungry_students = self.hungry_students + 1
			end
			students.remove(s)
		end
	end
end

function st:mousepressed (x, y, button)
	if self.cookie_box:contains(mouse.x, mouse.y) then
		cookie_in_hand = true
	else 
		cookie_in_hand = false
	end

	if not cookie_in_hand then
		slap.start_time = love.timer.getTime()
		slap.start_pos = vector(x,y)
	end
end

function st:mousereleased (x, y, button)
	if not cookie_in_hand then
		local slap_duration = love.timer.getTime() - slap.start_time
		slap.end_pos = vector(x,y)

		if slap_duration < 0.3 and (slap.end_pos - slap.start_pos):len() > 100 then
			local was_slapped = false
			for i,s in pairs(students.items) do
				if not s.is_slapped and s:isSlapped(slap.start_pos, slap.end_pos) then
					was_slapped = true
					self.signals:emit('slap', s, slap.start_pos, slap.end_pos)
					break
				end
			end

			if not was_slapped then
				self.signals:emit('whip', slap.start_pos, slap.end_pos)
			end
		end

		slap.start_time = 0
		return
	end

	cookie_in_hand = false
	for i,s in pairs(students.items) do
		if s:contains(x,y) then
			self.signals:emit('got_cookie', s)
			return
		end
	end
end

function st:keyreleased(key)
	if key == 'escape' then
		love.audio.play(Sound.static.whip1)
		GS.switch(State.menu)
	end
end

return st

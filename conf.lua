function love.conf(t)
	t.title             = "Lunchlady Simulator 2000 (by @fysxdotorg)"
	t.author            = "Martin Felis"
	t.url               = "http://fysx.org"
	t.identity          = "cookie_time"
	t.release           = true

	t.window.fullscreen = false
	t.window.resizable = true 
	t.window.minwidth  = 1000
	t.window.minheight = 700
end

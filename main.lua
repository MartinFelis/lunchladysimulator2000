require "strict"

class_commons = false 
common = nil

class      = require 'hump.class'
Timer      = require 'hump.timer'
vector     = require 'hump.vector'
Camera     = require 'hump.camera'
GS         = require 'hump.gamestate'
Signal     = require 'hump.signal'
Interrupt  = require 'interrupt'
Input      = require 'input'
Gui        = require "Quickie"
Polygon    = require 'HardonCollider.polygon'
students   = require 'students'

SCREEN_HEIGHT = 0
SCREEN_WIDTH = 0

-- evil global variables
GVAR = {
}

debug_variables = {
}

highscore = 0

function serialize(t, indent)
	local result = ""
	indent = "  " or indent
	for k,v in pairs(t) do
		if type(v) == "table" then
			result = result .. (indent .. " " .. k .. " = table (" .. tostring(v) .. "):") .. "\n"
			result = result .. serialize (v, indent .. "  ") .. "\n"
		else
			result = result .. (indent .. " " .. k .. " = " .. tostring(v)) .. "\n"
		end
	end

	return result
end

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function select_sound_source (sources)
	local source_index = math.floor((love.math.random() * #sources) + 1)
	if sources[source_index]:tell() == 0 then
		return sources[source_index]
	end

	for i,s in ipairs(sources) do
		local index = ((i + source_index) % #sources) + 1
		if sources[index]:tell() == 0 then
			return sources[index]
		end
	end

	sources[source_index]:rewind()
	return sources[source_index]
end



-- shallow copy
function table.copy(t)
	local r = {}
	for k,v in pairs(t) do r[k] = v end
	return r
end

-- proxies
local function Proxy(f)
	return setmetatable({}, {__index = function(t,k)
		local v = f(k)
		t[k] = v
		return v
	end})
end

-- e.g. GS.switch(State.menu)
State  = Proxy(function(path) return require('states.' .. path) end)

-- e.g. Entity.pawn(x,y) -- spawns pawn at x,y
Entity = Proxy(function(path) return require('entities.' .. path) end)

-- e.g. love.graphics.draw(Image.car, self.x, self.y)
Image  = Proxy(function(path)
	local i = love.graphics.newImage('img/'..path..'.png')
	i:setFilter('nearest', 'nearest')
	return i
end)

-- e.g. love.graphics.setFont(Font[30])
--      love.graphics.setFont(Font.fontface[20])
Font = Proxy(function(arg)
	if tonumber(arg) then
		return love.graphics.newFont(arg)
	end
	return Proxy(function(size) return love.graphics.newFont('font/'..arg..'.ttf', size) end)
end)

-- e.g. Sound.static.splatter:play()
--      Sound.stream.music:play()
Sound = {
	static = Proxy(function(path) return love.audio.newSource('snd/'..path..'.ogg', 'static') end),
	stream = Proxy(function(path) return love.audio.newSource('snd/'..path..'.ogg', 'stream') end)
}

debug_font = love.graphics.newFont(12)
score_font = Font.Hachicro[25]
points_font = Font.Hachicro[25]
instruction_font = Font.Hachicro[35]

function love.quit()
	-- save state etc
end

function love.load()
	local cookiebox = Entity.cookiebox

	Sound.stream.lunchtime:play()
	Sound.stream.lunchtime:setLooping(true)
	Sound.stream.lunchtime:setVolume(0.6)

	GS.registerEvents()
	--GS.switch(State.splash)
	GS.switch(State.menu)
--	GS.switch(State.game)
end

function love.resize(x,y)
	if GS.current().resize then
		GS.current():resize(x,y)
	end
end

function love.update(dt)
	Input.update()
	Timer.update(dt)
end

function Input.mappingDown(mapping, mag)
	GS.mappingDown(mapping, mag)
end

function Input.mappingUp(mapping, mag)
	GS.mappingUp(mapping, mag)
end

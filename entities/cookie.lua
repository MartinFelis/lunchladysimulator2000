local cookie = class{
	name = "Cookie",
	__includes = Entity.BaseEntity,
	init = function (self, pos, angle)
		Entity.BaseEntity.init (self, pos, angle)

		self.visual = Image.cookie
	end
}

return cookie

local base_entity = class{
	name = "BaseEntity",
	init = function (self, pos, angle)
		self.pos = pos or vector (0,0)
		self.vel = vector (0,0)
		self.angle = angle or 0 
		self.color = { 255., 255., 255. }
	end
}

function base_entity:draw()
	love.graphics.setColor (unpack(self.color))
	
	love.graphics.draw(self.visual,
	self.pos.x, self.pos.y, self.angle,
	1, 1,
	self.visual:getWidth() * 0.5,
	self.visual:getHeight() * 0.5)
end

function base_entity:finalize()
end

return base_entity
--]]

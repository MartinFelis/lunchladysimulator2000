local function select_style (styles)
	return styles[math.floor((love.math.random() * #styles) + 1)]
end

local student = class{
	name = "student",
	__includes = Entity.BaseEntity,
	init = function (self, pos, angle)
		Entity.BaseEntity.init (self, pos, angle)

		local window_size = vector(love.window.getDimensions())
		self.size = vector(window_size.x * 0.1, window_size.x * 0.3)
		self.size = vector(200, 200)

		self.force = vector(0,0)
		self.is_evil = false

		self.greed_factor = 1.
		self.default_velocity = 100

		self.is_happy = false
		self.got_cookie = false
		self.is_slapped = false
		self.arm_angle = 0 

		local hairstyles = {
			Image.face_hair1,
			Image.face_hair2,
			Image.face_hair3,
		}

		self.hair = select_style(hairstyles)

		local skin_colors = {
			{ 240, 210, 210 },
			{ 255, 228, 228 },
			{ 255, 200, 200 },
--			{ 184, 132, 76 },
		}

		self.skin_color = select_style(skin_colors)
		self.head_radius = 100

		local shirt_colors = {
			{255, 100, 100},
			{100, 255, 100},
			{100, 100, 255},

			{255, 255, 100},
			{255, 100, 255},
			{100, 255, 255},
		}

		self.shirt_color = select_style(shirt_colors)

		local hair_colors = {
			{240, 240, 128},
			{200, 200, 90},
			{100, 50, 0},
			{140, 90, 0},
		}

		self.hair_color = select_style(hair_colors)
	end,

	contains = function (self, x, y)
		local polygon = Polygon(
			-self.size.x * 0.5, -self.size.y *0.5, 
			-self.size.x * 0.5, self.size.y * 0.5,
			self.size.x * 0.5, self.size.y * 0.5,
			self.size.x * 0.5, -self.size.y * 0.5
		)
		polygon:move(self.pos.x, self.pos.y)
		polygon:rotate(self.angle)
		return polygon:contains(x,y)
	end,

	isSlapped = function (self, slap_start, slap_end)
		local head_center = vector(0., -self.size.y * 0.9)
		head_center:rotate_inplace (self.angle)
		head_center = head_center + self.pos

		-- kind of computing the distance of line segment to head center
		local len2 = (slap_end - slap_start):len2()
		if len2 < 1.0e-4 then
			return (slap_start - head_center):len()
		end

		local t = ((head_center - slap_start) * (slap_end - slap_start)) / len2

		if t > 1. then
			return false
		elseif t < 0. then
			return false
		end

		love.graphics.setColor (0, 0, 255)
		local closest_point = slap_start + t * (slap_end - slap_start)

		if (head_center - closest_point):len() < self.head_radius * 0.5 then
			return true
		end

		return false
	end,

	initPhysics = function (self, world)
		self.body = love.physics.newBody (world, self.pos.x, self.pos.y, "dynamic")
		self.shape = love.physics.newRectangleShape (self.size.x, self.size.y)
--		self.body:setAngle(2.)

--[[
		local frac = 0.3;
		local bottom = self.size.y * 0.5
		local top = bottom - self.size.y * frac
		local left = -self.size.x * 0.5
		local right = self.size.x * 0.5

		self.shape = love.physics.newPolygonShape (
			0, top,
			left, top - frac * 0.33,
			left, top - frac * 0.66,
			left * 0.33, bottom,
			right * 0.33, bottom,
			right, top - frac * 0.66,
			right, top - frac * 0.33,
			0, top
			)
			]]--
		self.fixture = love.physics.newFixture (self.body, self.shape)
	end,

	finalize = function (self)
		self.body:destroy()
	end,

	updateFromPhysics = function (self)
		self.pos.x, self.pos.y = self.body:getPosition()
		self.angle = self.body:getAngle()

		self.vel.x, self.vel.y = self.body:getLinearVelocity()
	end,

	setCookiePosition = function(self, x, y)
		local direction = self:getShoulderPosition() - vector(x,y)

--		local arm = vector(-self.size.x * 0.1, self.size.y * 0.7):rotate_inplace (self.angle + self.arm_angle)

		self.arm_angle = -self.angle + math.atan2(direction.y, direction.x) + 3.14159 * 0.5
	end,

	setDefaultVelocity = function (self)
		if self.is_happy then
			self.greed_factor = 2.
		end

		self.force.x = self.default_velocity * self.greed_factor
	end,

	getShoulderPosition = function (self)
		local shoulder = vector(self.size.x * 0.4, -self.size.y * 0.5):rotate_inplace (self.angle)

		return self.pos + shoulder
	end,

	getHandPosition = function (self)
		local shoulder = self:getShoulderPosition()
		local arm = vector(0, self.size.y * 0.7):rotate_inplace(self.angle):rotate_inplace(self.arm_angle)

		return shoulder + arm
	end,

	getHeadPosition = function (self)
		local neck = vector(self.size.x * 0., -self.size.y * 1.5):rotate_inplace (self.angle)

		return self.pos + neck
	end,

	update = function (self, dt)
		if not self.is_slapped then
			self:updateFromPhysics()
		end

		if math.abs(self.arm_angle) > 0.01 then
			local dir = 1.
			if self.arm_angle > 0 then
				dir = -1
			end

			self.arm_angle = self.arm_angle + dir * dt
		end

		if self.force:len2() > 1.0e-3 then
			self.body:setLinearVelocity (self.force.x, self.vel.y)
		end
	end,

	draw = function(self)
		love.graphics.push()
		love.graphics.translate(self.pos.x, self.pos.y)
		love.graphics.rotate(self.angle)
		love.graphics.setColor(unpack(self.shirt_color))

		local body_draw_scale = vector(
			self.size.x / Image.body:getWidth(),
			self.size.y * 0.9 / Image.body:getHeight()
		)

		love.graphics.draw(Image.body, -self.size.x * 0.5, -self.size.y * 0.5, 0, body_draw_scale.x, body_draw_scale.y)

--		love.graphics.rectangle ("fill", -self.size.x * 0.5, -self.size.y * 0.5, self.size.x, self.size.y)
		love.graphics.setColor(180., 180., 180.)
--		love.graphics.rectangle ("fill", -self.size.x * 0.45, -self.size.y * 0.45, self.size.x * 0.9, self.size.y * 0.9)

		love.graphics.pop()

		if self.is_slapped then
			self:drawArmsAndHead()
		end
	end,

	drawArmsAndHead = function(self)
		love.graphics.push()
		love.graphics.translate(self.pos.x, self.pos.y)
		love.graphics.rotate(self.angle)

		-- draw the arm
		love.graphics.push()
		love.graphics.translate(self.size.x * 0.25, -self.size.y * 0.35)
		love.graphics.rotate(self.arm_angle)

		love.graphics.setColor(unpack(self.shirt_color))

		if self.is_evil or (not self.is_slapped and not self.is_happy) then
-- debug box			
--			love.graphics.rectangle ("fill", -self.size.x * 0.15, self.size.y * 0., self.size.x * 0.3, self.size.y * 0.7)

			love.graphics.setColor (unpack(self.skin_color))
--			love.graphics.circle("fill", 0., self.size.y * 0.7, self.head_radius * 0.4, 20)

			-- draw the arm
			local arm_draw_scale = vector(
				(self.size.x * 0.3)/ Image.arm:getWidth(),
				(self.size.y * 0.6)/ Image.arm:getHeight()
			)

			love.graphics.setColor (unpack(self.shirt_color))
			love.graphics.draw(Image.arm, -self.size.x * 0.15, 0., 0, arm_draw_scale.x, arm_draw_scale.y)

			-- draw the hand
			local hand_draw_scale = vector(
				(self.head_radius * 0.7)/ Image.hand:getWidth(),
				(self.head_radius * 0.7)/ Image.hand:getHeight()
			)

			love.graphics.setColor (unpack(self.skin_color))
			love.graphics.draw(Image.hand, - self.head_radius * 0.35, self.size.y * 0.6, 0, hand_draw_scale.x, hand_draw_scale.y)
		end
		love.graphics.pop()

		-- draw the head


--		love.graphics.circle ("fill", 0, -self.size.y * 0.9, self.head_radius, 20)

		local face_draw_scale = vector(
			( self.head_radius * 3.) / Image.face:getWidth(),
			( self.head_radius * 3.) / Image.face:getHeight()
		)

		if self.is_slapped then
			love.graphics.setColor (unpack(self.skin_color))
			love.graphics.draw(Image.face_slapped, -self.head_radius * 1.5, -self.size.y * 1.75, 0, face_draw_scale.x, face_draw_scale.y)
			love.graphics.setColor (255, 255, 255)
			love.graphics.draw(Image.face_slapped_deco, -self.head_radius * 1.5, -self.size.y * 1.75, 0, face_draw_scale.x, face_draw_scale.y)
		else
			love.graphics.setColor (unpack(self.skin_color))
			love.graphics.draw(Image.face, -self.head_radius * 1.5, -self.size.y * 1.75, 0, face_draw_scale.x, face_draw_scale.y)
			love.graphics.setColor (255, 255, 255)
			love.graphics.draw(Image.face_deco, -self.head_radius * 1.5, -self.size.y * 1.75, 0, face_draw_scale.x, face_draw_scale.y)
		end

		love.graphics.setColor (unpack(self.hair_color))
		love.graphics.draw(self.hair, -self.head_radius * 1.5, -self.size.y * 1.75, 0, face_draw_scale.x, face_draw_scale.y)

		love.graphics.pop()

	end,

}

return student

local cookiebox = class{
	name = "CookieBox",
	__includes = Entity.BaseEntity,
	init = function (self, pos, angle)
		Entity.BaseEntity.init (self, pos, angle)

		local window_size = vector(love.window.getDimensions())
		self.size = vector(window_size.x * 0.4, window_size.x * 0.3)
		self.color = {255, 255, 128}
	end,

	contains = function (self, x, y)
		local polygon = Polygon(
			-self.size.x * 0.5, -self.size.y *0.5, 
			-self.size.x * 0.5, self.size.y * 0.5,
			self.size.x * 0.5, self.size.y * 0.5,
			self.size.x * 0.5, -self.size.y * 0.5
		)
		polygon:move(self.pos.x, self.pos.y)
		polygon:rotate(self.angle)
		return polygon:contains(x,y)
	end,

	update = function (self, dt) 
		local window_size = vector(love.window.getDimensions())
		self.size = vector(window_size.x * 0.25, window_size.x * 0.18)
	end,

	draw = function(self)
		love.graphics.push()
		love.graphics.translate(self.pos.x, self.pos.y)
		love.graphics.rotate(self.angle)
		love.graphics.setColor(unpack(self.color))

		local cookiebox_draw_scale = vector(
		self.size.x / Image.cookiebox:getWidth(),
		self.size.y / Image.cookiebox:getHeight()
		)

		love.graphics.setColor (255, 255, 255)
		love.graphics.draw(Image.cookiebox, -self.size.x * 0.5, - self.size.y * 0.5, 0, cookiebox_draw_scale.x, cookiebox_draw_scale.y)

--		love.graphics.rectangle ("fill", -self.size.x * 0.5, -self.size.y * 0.5, self.size.x, self.size.y)
--		love.graphics.setColor(180., 180., 180.)
--		love.graphics.rectangle ("fill", -self.size.x * 0.45, -self.size.y * 0.45, self.size.x * 0.9, self.size.y * 0.9)
		love.graphics.pop()
	end
}

return cookiebox
